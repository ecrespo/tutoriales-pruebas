#!/usr/bin/env python
#Se importa sqrt de math
from math import sqrt


#Clase calculadora
class Calculadora:
    #Metodo suma de x y y, se evalua si son enteros si no, devuelve error.
    def suma(self,x,y):
        if type(x) == int and type(y) == int:
            return x + y
        else:
            raise TypeError("Invalid type: {} and {}".format(type(x),type(y)))

    #Metodo raizcuadrada de X, devuelve la raiz cuadrada si es entero positivo, si no
    #devuelve mensaje de error.
    def raizCuadrada(self,x):
        if type(x) == int and x >= 0:
            return math.sqrt(x)
        else:
            raise TypeError("Invalid type: {} ".format(type(x)))


if __name__ == '__main__':
    #Se crea una instancia de la clase
    calc = Calculadora()
    #Se calcula la suma de 2 y 2, se muestra en pantalla.
    results = calc.sum(2, 2)
    print (results)

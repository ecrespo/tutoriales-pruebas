FROM python
WORKDIR /code/

RUN pip3 install --upgrade pip
RUN pip3 install nose
RUN pip3 install nose-cov
RUN pip3 install rednose
RUN pip3 install pytest
RUN pip3 install pytest-cov
RUN pip3 install mock


#EXPOSE 5000

ADD . /code/
COPY . /code/


CMD nosetests  --with-coverage
#CMD nosetests -sv --rednose

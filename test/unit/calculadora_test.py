#!/usr/bin/env python3
import unittest
from app.calculadora import Calculadora



class TestCalculadora(unittest.TestCase):
    def setUp(self):
        self.calc = Calculadora()

    def test_suma_retorna_resultado_correcto(self):
        ##Asegura que sea igual la operacion de suma 2+2 a 4
        self.assertEqual(4, self.calc.suma(2,2))

    def test_suma_devuelve_error_si_el_tipo_no_es_entero(self):
        #Asegura error, si es de tipo, cuando se le pasa dos string.
        self.assertRaises(TypeError, self.calc.suma, "Hello", "World")


    def test_asegura_que_sea_verdadero(self):
        #Se asegura que el valor 1 es true y un string.
        self.assertTrue(1)
        self.assertTrue("Hello, World")

    def test_aseura_que_sea_falso(self):
        #Se asegura que el string vacio y cero son falso.
        self.assertFalse(0)
        self.assertFalse("")

    def test_asegura_que_es_mayor(self):
        #Se asegura que 2>1
        self.assertGreater(2, 1)

    def test_asegura_que_es_mayor_e_igual(self):
        #Se asegura que 2>=2
        self.assertGreaterEqual(2, 2)


    def test_asegura_que_es_casi_igual_a_delta_0_5(self):
        #Se asegura que sea casi igual 1 y 1.2 con delta de 0.5
        self.assertAlmostEqual(1, 1.2, delta=0.5)

    def test_asegura_lugares_casi_igual(self):
        #Se asegura que es casi igual 1 y 1.00001 por 4 lugares.
        self.assertAlmostEqual(1, 1.00001, places=4)


    def test_asegura_diccionario_contiene_subconjunto(self):
        esperado = {'a': 'b'}
        actual = {'a': 'b', 'c': 'd', 'e': 'f'}
        #Se asegura que el diccionario actual contiene lo esperado.
        self.assertDictContainsSubset(esperado, actual)


    def test_asegura_diccionarios_iguales(self):
        esperado = {'a': 'b', 'c': 'd'}
        actual = {'c': 'd', 'a': 'b'}
        #Se asegra que el diccionario esperado sea igual al actual.
        self.assertDictEqual(esperado, actual)


    def test_asegura_que_esta_en(self):
        #Se asegura que 1 este en la lista
        self.assertIn(1, [1,2,3,4,5])

    def test_asegura_expresiones_iguales(self):
        #Se asegura que las expresiones son iguales expre1 y expre2
        self.assertIs("a", "a")

    def test_asegura_objeto_es_instancia_de_una_clase(self):
        #Se asegura que el objeto 2 sea de la clase entero
        self.assertIsInstance(2, int)

    def test_asegura_objeto_no_es_instancia_de_una_clase(self):
        #Se asegura que el objeto 2 no sea una clase str
        self.assertNotIsInstance(2, str)

    def test_asegura_que_es_none(self):
        #Se asegura que es None.
        self.assertIsNone(None)

    def test_asegura_expresiones_no_sean_iguales(self):
        self.assertIsNot([], [])

    def test_asegura_que_no_sea_none(self):
        #Se asegura que 1 no es None.
        self.assertIsNotNone(1)

    def test_asegura_que_es_menor(self):
        #Se asegura que 3 es menor que 5
        self.assertLess(3, 5)

    def test_asegura_que_es_menor_e_igual(self):
        #Se asegura que 7 es menor o igual que 7.
        self.assertLessEqual(7, 7)



if __name__ == '__main__':
    unittest.main()
